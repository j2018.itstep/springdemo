package by.itstep.j2018.spring.demo.component;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
@Scope("singleton" )
public class GsonComponent{
	private Gson gson = new Gson();
	
	public String createJson(Object object) {
		return gson.toJson(object);
	}
}
