package by.itstep.j2018.spring.demo.component;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class TestComponent {
	private Random random = new Random();
	
	static{
		System.out.println("Static initialisation test component");
	}
	
	{
		System.out.println("Not static initialisation test component");
	}
	
	public int getSuperNumber() {
		return random.nextInt(2)*100;
	}

}
