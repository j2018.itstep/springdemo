package by.itstep.j2018.spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import by.itstep.j2018.spring.demo.data.CityRepository;
import by.itstep.j2018.spring.demo.entity.City;

@Controller
@RequestMapping(value = "/city")
public class CityController{
	@Autowired
	private CityRepository cityRepository;
	
	@RequestMapping(path = "/{name}")
	public String hello(@PathVariable("name") String name, Model model) {
		var city = cityRepository.findCityByName(name);
		city.ifPresentOrElse(value -> {
			model.addAttribute(value);
		}, () -> {
			model.addAttribute(new City());
		});
		return "city";
	}

}
