package by.itstep.j2018.spring.demo.controller;

import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import by.itstep.j2018.spring.demo.component.GsonComponent;
import by.itstep.j2018.spring.demo.data.CityRepository;
import by.itstep.j2018.spring.demo.error.ErrorJsonRequest;

@Controller
@RequestMapping(value = "/country")
public class CountryController {
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private GsonComponent gson;

	@ResponseBody
	@RequestMapping(path = "/{code}")
	public String getCitiesByCountryCode(@PathVariable("code") String code, HttpServletRequest request,
			HttpServletResponse response) {
		response.addHeader("Content-Type", "text/html; charset=utf-8");
		var cities = cityRepository.findCitiesByCountryCode(code);
		return gson.createJson(cities);
	}
	
	public String getCityByCountyCode(@PathVariable("code") String code, HttpServletRequest request,
			HttpServletResponse response) {
		AtomicReference<String> ar = new AtomicReference<String>();
		cityRepository.findCityByName("Mins").ifPresentOrElse(value -> {
			ar.set(gson.createJson(value));
		}, () -> {
			ar.set(gson.createJson(new ErrorJsonRequest("No such sity")));
		});
		return ar.get();
	}
	
}
