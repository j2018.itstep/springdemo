package by.itstep.j2018.spring.demo.controller;

import java.net.http.HttpRequest;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import by.itstep.j2018.spring.demo.component.GsonComponent;
import by.itstep.j2018.spring.demo.component.TestComponent;
import by.itstep.j2018.spring.demo.data.CityRepository;
import by.itstep.j2018.spring.demo.data.CountryRepository;
import by.itstep.j2018.spring.demo.error.ErrorJsonRequest;

@Controller
public class MainController {
	@Autowired
	private TestComponent testComponent;
	@Autowired
	private CountryRepository countryRepository;
	
	static {
		System.out.println("Static initialisation test controller");
	}

	static {
		System.out.println("Not static initialisation test controller");
	}

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String test(HttpServletRequest request, HttpServletResponse response, Model model) {
		var countries = countryRepository.getCountryAll();
		model.addAttribute("countries", countries);
		return "index";
	}

}
