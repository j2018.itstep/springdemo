package by.itstep.j2018.spring.demo.data;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.itstep.j2018.spring.demo.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer>{
	public Optional<City> findCityByName(String name);
	public List<City> findCitiesByCountryCode(String code);
}
