package by.itstep.j2018.spring.demo.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import by.itstep.j2018.spring.demo.entity.Country;

@Repository
public class CountryRepository {
	private final static String SQL = "SELECT `Code`, `Name`, `Population`, `Region` FROM `country` WHERE `Code` LIKE '%s'";
	private final static String SQL2 = "SELECT `Code`, `Name`, `Population`, `Region` FROM `country`";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Country getCountryByCode(String code) {
		List<Country> countries = jdbcTemplate.query(String.format(SQL, code), new RowMapper<Country>() {

			@Override
			public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
				Country country = new Country();
				country.setCode(rs.getString(1));
				country.setName(rs.getString(2));
				country.setPopulation(rs.getInt(3));
				country.setRegion(rs.getString(4));
				return country;
			}
			
		});
		return countries.get(0);
	}
	
	public List<Country> getCountryAll() {
		List<Country> countries = jdbcTemplate.query(SQL2, new RowMapper<Country>() {

			@Override
			public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
				Country country = new Country();
				country.setCode(rs.getString(1));
				country.setName(rs.getString(2));
				country.setPopulation(rs.getInt(3));
				country.setRegion(rs.getString(4));
				return country;
			}
			
		});
		return countries;
	}
}
