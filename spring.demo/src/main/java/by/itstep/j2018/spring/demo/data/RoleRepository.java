package by.itstep.j2018.spring.demo.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.itstep.j2018.spring.demo.security.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	
}
