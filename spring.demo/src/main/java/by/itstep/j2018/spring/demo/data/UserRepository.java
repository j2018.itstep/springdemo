package by.itstep.j2018.spring.demo.data;
 
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import by.itstep.j2018.spring.demo.entity.City;
import by.itstep.j2018.spring.demo.security.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	public Optional<User> findUserByUserName(String name);
	public Optional<User> findUserByEmail(String code);
}
