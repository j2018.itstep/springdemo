package by.itstep.j2018.spring.demo.entity;

import javax.persistence.*;

@Entity
public class TourObject {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@ManyToOne(targetEntity = City.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private City city;
	
}
