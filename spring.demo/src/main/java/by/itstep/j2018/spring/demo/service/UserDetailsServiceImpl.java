package by.itstep.j2018.spring.demo.service;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import by.itstep.j2018.spring.demo.data.UserRepository;
import by.itstep.j2018.spring.demo.security.model.Role;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AtomicReference<User> user = new AtomicReference<>();
		userRepository.findUserByUserName(username).ifPresentOrElse((userValue) -> {
			Set<GrantedAuthority> authorities = new HashSet<>();
			for (Role role: userValue.getRoles()) {
				authorities.add(new SimpleGrantedAuthority(role.getName()));
			}
			user.set(new User(userValue.getUserName(), userValue.getPassword(), authorities));
		}, () -> {
			throw new UsernameNotFoundException(String.format("User %s not found", username));
		});
		return user.get();
	}

}
