$("document").ready(function() {
	console.log("start");
	$("#countries").on("change", function() {
		getCountryDetails(this.value)
	});
});

function getCountryDetails(code) {
	let url = "http://localhost:8080/country/" + code
	console.log(url)
	$.ajax(url).done(function(answer) {
		updateCities(JSON.parse(answer))
	}).fail(function() {
		alert("error");
	})
};

function updateCities(cities) {
	let myNode = $("#blue")
	myNode.empty();
	let table = document.createElement("table");
	table.setAttribute("id", "cityTable")
	for (index in cities) {
		console.dir(cities[index])
		let row = document.createElement("tr");
		let name = document.createElement("td");
		let link = document.createElement("a")
		link.setAttribute("href", "/city/" + cities[index].name)
		link.innerHTML = cities[index].name;
		name.append(link);
		row.append(name);
		let population = document.createElement("td");
		population.innerHTML = cities[index].population;
		row.append(population);
		table.append(row);
	};
	myNode.append(table);
};

